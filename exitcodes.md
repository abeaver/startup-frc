# 1000s - Filesystem Errors
## 1000-1099
### 1000
> Error interacting with filesystem

### 1001
> Cannot find current directory

### 1002
> Cannot change directories

### 1003
> Cannot create new directory

### 1004
> Cannot create new file

### 1005
> Could not save data to file

# 2000s - Templating Errors
## 1000-1099
### 2001
> Failure when parsing a template